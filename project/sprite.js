console.log("Sprite File is Connected");
function drawfruit(x, y) {
    let fruitString = '<g transform="translate(' + x + ' ' + y + ')">';
    let circleString = '<circle cx="100" cy="100" r="50" fill="green"/>';
    let lineString = '<line x1="20" y1="10" x2="18" y1="0" stroke="black" />';
    let gString = '</g>';
    fruitString += circleString + lineString + gString;
    return fruitString;
}
window.drawfruit = drawfruit;
console.log(drawfruit(200, 200));
console.log(drawfruit(100, 200));
console.log(drawfruit(200, 300));
function drawanimal(x, y) {
    let animalString = '<g transform="translate(' + x + ' ' + y + ')">';
    let ellipseString = '<ellipse cx="60" cy="15" ry="5" rx="10" />';
    let circleString = ' <circle cx="47" cy="15" r="2" />';
    let lineString = ' <line x1="55" y1="20" x2="55" y2="25" stroke="black"/>';
    let line2String = '<line x1="65" y1="20" x2="65" y2="25" stroke="black"/>';
    let gString = '</g>';
    animalString += ellipseString + circleString + lineString + line2String + gString;
    return animalString;
}
window.drawanimal = drawanimal;
console.log(drawanimal(200, 200));
console.log(drawanimal(100, 200));
console.log(drawanimal(200, 300));
function drawvehicle(x, y) {
    let vehicleString = '<g transform="translate(' + x + ' ' + y + ')">';
    let polygonString = '<polygon points="100,15 110,15 95,20 115,20" fill="red"/>';
    let circleString = '<circle cx="100" cy="22" r="3" />';
    let circle2String = '<circle cx="110" cy="22" r="3" />';
    let gString = '</g>';
    vehicleString += polygonString + circleString + circle2String + gString;
    return vehicleString;
}
window.drawvehicle = drawvehicle;
console.log(drawvehicle(200, 200));
console.log(drawvehicle(100, 200));
console.log(drawvehicle(200, 300));
function drawplant(x, y) {
    let plantString = '<g transform="translate(' + x + ' ' + y + ')">';
    let circleString = '<circle cx="145" cy="17" r="5" fill="green" />';
    let circle2String = '<circle cx="155" cy="17" r="5" fill="green"/>';
    let circle3String = '<circle cx="150" cy="10" r="5" fill="green"/>';
    let lineString = '<line x1="150" y1="20" x2="150" y2="30" stroke="black"/>';
    let gString = '</g>';
    plantString += circleString + circle2String + circle3String + lineString + gString;
    return plantString;
}
window.drawplant = drawplant;
console.log(drawplant(200, 200));
console.log(drawplant(100, 200));
console.log(drawplant(200, 300));







// defining the variables and objects
let grid = [];
let canvas = document.querySelector("#canvas");
let background = document.querySelector("#background");
let object = document.querySelector("#object");
let sharedKeyVariable = "";
let x = 0;
let y = 0;
let results = document.querySelector("#results");
let score = 0;
let timer = setInterval(fallingobject, 500);
let i = 0;
let objects = [];
//generating virtual board 
function gamearea(height, width) {
    for (let row = 0; row < height; row++) {
        grid.push([]);
        for (let col = 0; col < width; col++) {
            grid[row].push("E");
        }
    }
    return grid;
}
let board = gamearea(10, 10);
console.log(board);
// printing the board on screen
function print(height, width, board) {
    let svgString = " ";
    for (let row = 0; row < height; row++) {
        for (let col = 0; col < width; col++) {
            if (grid[row][col] == "O") {
                svgString += '<rect x="' + col * 20 + '" y="' + row * 20 + '" width="18" height="18" fill="red"/>';
            } else if (grid[row][col] == "E") {
                svgString += '<rect x="' + col * 20 + '" y="' + row * 20 + '" width="18" height="18" fill="blue"/>';
            }

        }
    }
    background.innerHTML = svgString;
}
console.log(print(10, 10));
// eventlistenrer to move the objects
function move(event) {
    console.log("recieved key", event.key);
    if (event.key == "d" && x < 180) {
        x = x + 20;
    }
    else if (event.key == "a" && x > 0) {
        x = x - 20;
    } else if (event.key == "w" && 0 < y < 180) {
        rotate();
    }
}
// helper function to rotate the object from one shape to another
function rotate() {
    if (shapes[r] == 3) {
        r = 1;
    } else if (shapes[r] == 2) {
        r = 2;
    }

}
// defining diiferent shapes
function createShape1(x, y) {
    rectString = '<rect x="' + x + '" y="' + y + '" height="20" width="20" fill="black"/>';
    return rectString;
}
//creating objects with 2 squares
function createShape2(x, y) {
    rectString = '<rect x="' + x + '" y="' + y + '" height="20" width="20" fill="black"/>';
    rect2String = '<rect x="' + (x - 20) + '" y="' + y + '" height="20" width="20" fill="black"/>';
    rectString += rect2String;
    return rectString;
}
//again creating objects with 2 squares but in different order
function createShape3(x, y) {
    rectString = '<rect x="' + x + '" y="' + y + '" height="20" width="20" fill="black"/>';
    rect2String = '<rect x="' + x + '" y="' + (y - 20) + '" height="20" width="20" fill="black"/>';
    rectString += rect2String;
    return rectString;
}
let create = createShape3(20, 20);
console.log(create);
let shapes = [1, 2, 3];
var r = 0;
//the game loop
function fallingobject() {
    i = (i + 1) % 10;
    let yArray = [0, 20, 40, 60, 80, 100, 120, 140, 160, 180];
    let xArray = [0, 20, 40, 60, 80, 100, 120, 140, 160, 180];
  // choosing a random shape in a random column
    if (shapes[r] == 1) {
        let rectString = createShape1(x, yArray[i]);
        object.innerHTML = rectString;
    }
    else if (shapes[r] == 2) {
        let rectString = createShape2(x, yArray[i]);
            object.innerHTML = rectString;
    }
    else if (shapes[r] == 3) {
        let rectString = createShape3(x, yArray[i]);
        object.innerHTML = rectString;
    }
    let xn = x / 20;
    // checking for the interference
    if (yArray[i] == 180 || grid[(i + 1) % 10][xn] == 'O') {

        if (shapes[r] == 1 ) {
            grid[i][xn] = "O";
        }
        else if (shapes[r] == 2 &&  grid[(i + 1) % 10][(xn - 1)] == "O") {
           console.log("running");
            grid[i][xn] = "O";
            grid[i][(xn - 1)] = "O";
        }
        else if(shapes[r] == 2){
//             console.log((i+1),(xn-1));
//            console.log((i+1),xn);
              grid[i][xn] = "O";
            grid[i][(xn - 1)] = "O";
        }
       else if (shapes[r] == 3) {
            grid[i][xn] = "O";
            grid[(i - 1)][xn] = "O";

        }
        r = Math.floor(Math.random() * shapes.length);
        let n = 0;
        n = Math.floor(Math.random() * yArray.length);
        i = 0;
        x = xArray[n];
        console.log(x, i);

// ending the game if there is no more space
        for (i = 0; i < 10; i++) {
            if (grid[0][i] == "O") {
                results.innerHTML = "YOU LOSE!!";
                canvas.innerHTML = "Refresh To Play Again";
                clearInterval(timer);
            }
        }
        
    }


    //checking if I need to clear one row
    //clearing rows if needed and moving the objects down
    checkingrows(grid);   
    print(10, 10);
}
function checkingrows(grid) {
    let count = 0;
    //checking if a row is complete or not
    for (let row = 0; row < 10; row++) {
        let removeCondition = true;
        for (column = 0; column < 10; column++) {
            removeCondition = removeCondition && (grid[row][column] == 'O');
        }
        //clearing rows if completed
        if (removeCondition) {
            remove(row);
            count += 1;

        }
    }
    //adding scores when row(s) is removed.
    if (count == 1) {
        score += 1;
    } else if (count > 1) {
        score += 10;
    }
    //printing the results
    result();
}
    // removing the completed rows
function remove(row) {
    console.log("removing");
    grid.splice(row, 1);
    //moving objects down
    grid.unshift(["E", "E", "E", "E", "E", "E", "E", "E", "E", "E"]);

}
// grid[4][4] = "O";
//printing the results
function result() {
    results.innerHTML = "YOUR SCORE " + score;
    if (score >= 50) {
        clearInterval(timer);
        results.innerHTML = "YOU WON!!";
        canvas.innerHTML = "Refresh To Play Again";
    }

}



