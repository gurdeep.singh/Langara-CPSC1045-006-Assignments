//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");
function update(){
let shape=document.querySelector("#collection");
let xmover=document.querySelector("#inputx");
let ymover=document.querySelector("#inputy");
let slider=document.querySelector("#rotator");
let translateString="translate("+Number(xmover.value)+ ","+Number(ymover.value)+")";
console.log(translateString);
let rotateString="rotate("+Number(slider.value)+")";
let transformString=translateString+"  "+rotateString;
console.log(transformString);
shape.setAttribute("transform",transformString);
}
window.update=update;
function changepolygon(){
  let star = document.querySelector("#star");
  let poly = document.querySelector("#poly");
  let xmove = document.querySelector("#xchange");
  let ymove = document.querySelector("#ychange");
  let translateString = "translate("+Number(xmove.value)+ "," +Number(ymove.value)+ ")";
  console.log(translateString);
  star.setAttribute("transform",translateString);
   var Ro = document.querySelector("#orchange");
  var Ri = document.querySelector("#irchange");
  var ro = Number(Ro.value);
  var ri = Number(Ri.value);
var a = Math.sin(0);
var b = Math.sin(0.76);
var c = Math.sin(1.57);
var d = Math.sin(2.36);
var e = Math.sin(3.14);
var f = Math.sin(3.92);
var g = Math.sin(4.71);
var h = Math.sin(5.50);
var i = Math.cos(0);
var j = Math.cos(0.76);
var k = Math.cos(1.57);
var l = Math.cos(2.36);
var m = Math.cos(3.14)
var n = Math.cos(3.92);
var o = Math.cos(4.71);
var p = Math.cos(5.50);
var points = ""+ro*i+","+ro*a+" "+ri*j+","+ri*b+" "+ro*k+","+ro*c+" "+ri*l+","+ri*d+" "+ro*m+","+ro*e+" "+ri*n+","+ri*f+" "+ro*o+","+ro*g+" "+ri*p+","+ri*h+"";
console.log(points);
poly.setAttribute("points",points);
}
window.changepolygon=changepolygon;