//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");
function boxes() {
    let count = document.querySelector("#boxcount");
    let size = document.querySelector("#size");
    let growth = document.querySelector("#change");
    let output = document.querySelector("#output");
    let count1 = Number(count.value);
    let size1 = Number(size.value);
    let growth1 = Number(growth.value);
    let svgstring = '<svg width="500" height="500">';
    let z = size1;
    let offset = 0;
    for (i = 1; i <= count1; i++) {
        let rectstring = '<rect x="' + offset + '" y="' + 50 + '" height="'+z+'" width="'+z+'" fill="black" />';
        svgstring += rectstring;
         z += growth1;
         offset += z + growth1+10;
        
    }
    svgstring += "</svg>";
    output.innerHTML = svgstring;
    console.log(svgstring);
}
window.boxes = boxes;