//This is the entry point JavaScript file.
//Webpack will look at this file first, and then check
//what files are linked to it.
console.log("Hello world");
function listener() {
    let shape = document.querySelector("#star");
    let input = document.querySelector("#points");
    let sides = Number(input.value);
    var innerR = 50;
    var outerR = 150;
    let pArray = helper1(sides,innerR,outerR);
    let pString = helper2(pArray);
    shape.setAttribute("points", pString);
    }

function helper1(sides, innerR, outerR) {
    let pointsArray = [];
    for (i = 0; i < (2 * sides); i++) {
    var point1 = { x: outerR * (Math.cos(i*((Math.PI)/sides))), y: outerR * (Math.sin(i*(Math.PI)/sides)) };
    var point2 = { x: innerR * (Math.cos(i*((Math.PI)/sides))), y: innerR * (Math.sin(i*(Math.PI)/sides)) };
     console.log(point1);
     console.log(point2);
        if (i % 2 == 0) {
            pointsArray.push(point1);
        }
        else {
            pointsArray.push(point2);
        }
       
    }
    console.log( pointsArray);
    return pointsArray;
}
function helper2(pointsArray) {
    let string = "";
    for (element of pointsArray) {
        string += (element.x + " , "+ element.y +" ");
    
    }
    console.log(pointsArray);
        return string;

}
window.helper1 = helper1;
window.helper2 = helper2;
window.listener = listener;